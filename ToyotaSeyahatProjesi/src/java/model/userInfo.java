package model;

import java.sql.Date;

public class userInfo {
    private int id;
    private int k_id; //giren usera göre bilgi aktarımı
    private String Department;
    private String Manager;
    private Date StartDate;
    private Date EndDate;
    private String Place;
    private String Target;
    private String P_Code;

    public userInfo(int id, int k_id, String department, String manager, Date startDate, Date endDate, String place, String target, String p_Code) {
        this.id = id;
        this.k_id = k_id;
        Department = department;
        Manager = manager;
        StartDate = startDate;
        EndDate = endDate;
        Place = place;
        Target = target;
        P_Code = p_Code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getK_id() {
        return k_id;
    }

    public void setK_id(int k_id) {
        this.k_id = k_id;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public String getManager() {
        return Manager;
    }

    public void setManager(String manager) {
        Manager = manager;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date startDate) {
        StartDate = startDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date endDate) {
        EndDate = endDate;
    }

    public String getPlace() {
        return Place;
    }

    public void setPlace(String place) {
        Place = place;
    }

    public String getTarget() {
        return Target;
    }

    public void setTarget(String target) {
        Target = target;
    }

    public String getP_Code() {
        return P_Code;
    }

    public void setP_Code(String p_Code) {
        P_Code = p_Code;
    }
}
