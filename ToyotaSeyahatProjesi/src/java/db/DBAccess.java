package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBAccess {

    static String url = "jdbc:sqlserver://localhost:1433;databaseName=ToyotaProjesi";
    static String user = "madmen";
    static String password = "123";
    static Connection con = null;

    public static Connection getConnection() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url, user, password);
            System.out.println("Connection Succesfully ın Database");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return con;
    }
    
    public static void main(String[] args) throws SQLException {
        DriverManager.getConnection(url,user,password);
    }
}
