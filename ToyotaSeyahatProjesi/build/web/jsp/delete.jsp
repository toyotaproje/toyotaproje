
<%@page import="db.DBAccess"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
    <title>Sil</title>
    </head>
    <body>

        <%
            String k_idd = request.getParameter("k_id");
            int k_id = Integer.parseInt(k_idd);
            Connection con = null;
            PreparedStatement pst = null;
            try {
                int row = 0;
                con = DBAccess.getConnection();
                String sorgu = "delete from userInfo where id=?";
                pst = con.prepareStatement(sorgu);
                pst.setInt(1, k_id);
                row = pst.executeUpdate();
                if (row > 0) {
                    response.sendRedirect("HomePage.jsp");
                } else {
                    out.println("Erron in query");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        %>
    </body>
</html>
