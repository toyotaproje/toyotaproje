
<%@page import="db.DBAccess"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Düzenle</title>
  <link type="text/css" href="../resources/css/required/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.structure.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.theme.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/css/required/mCustomScrollbar/jquery.mCustomScrollbar.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/css/required/icheck/all.css" rel="stylesheet" />
      <link type="text/css" href="../resources/fonts/metrize-icons/styles-metrize-icons.css" rel="stylesheet">

	<!-- Optional CSS Files -->
	<link type="text/css" href="../resources/css/optional/bootstrapValidator.min.css" rel="stylesheet" />
	<!-- add CSS files here -->

	<!-- More Required CSS Files -->
	<link type="text/css" href="../resources/css/styles-core.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/styles-core-responsive.css" rel="stylesheet" />
	</head>

	<body>
	  <%
           String k_id = request.getParameter("k_id");
           int k_ide = Integer.parseInt(k_id);
	  %>

	  <form action="editInfoBack.jsp?k_id=<%=k_id%>" method="post">
	    <%
             Connection con = null;
             PreparedStatement ps = null;
             ResultSet rs = null;

             try {
              con = DBAccess.getConnection();
              String sorgu = "select Department,Manager,StartDate,EndDate,Place,Target,P_Code from userInfo where id='" + k_ide + "'";
              ps = con.prepareStatement(sorgu);
              rs = ps.executeQuery();
	    %>
	    <%
             while (rs.next()) {
	    %>

	    <div id="right-column">
	      <div class="col-md-12">
		<div class="col-md-4"></div>
		  <div class="row">
		    <div class="col-md-4">
		      <div class="block">
			<div class="block-heading">
			  <div class="main-text h3">
			    Düzenle
			  </div>
			  <div class="block-controls">
			    <span aria-hidden="true" class="icon icon-arrow-down icon-size-medium block-control-collapse"></span>
			  </div>
			</div>
			<div class="block-content-outer">
			  <div class="block-content-inner">
			    <form id="registrationForm" method="post" action="addNewBack.jsp?k_id=<%=k_id%>">
			      <div class="form-group">
				<label class="control-label">Bölümü</label>
				<input type="text" class="form-control" name="Department" value="<%=rs.getString("Department")%>" required/>
			      </div>
			      <div class="form-group">
				<label class="control-label">Müdürü</label>
				<input type="text" class="form-control" name="Manager" value="<%=rs.getString("Manager")%>" required/>
			      </div>
			      <div class="form-group">
				<label class="control-label">Seyahat Başlangıç</label>
				<input type="date" class="form-control" name="StartDate" placeholder="DD/MM/YYYY" value="<%=rs.getDate("StartDate")%>" required/>
			      </div>
			      <div class="form-group">
				<label class="control-label">Seyahat Bitiş</label>
				<input type="date" class="form-control" name="EndDate" placeholder="DD/MM/YYYY" value="<%=rs.getDate("EndDate")%>" required/>
			      </div>
			      <div class="form-group">
				<label class="control-label">Seyahat Yeri</label>
				<input type="text" class="form-control" name="Place" value="<%=rs.getString("Place")%>" required/>
			      </div>
			      <div class="form-group">
				<label class="control-label">Gidiş Amacı</label>
				<input type="text" class="form-control" name="Target" value="<%=rs.getString("Target")%>" required/>
			      </div>
			      <div class="form-group">
				<label class="control-label">Proje Kodu</label>
				<input type="text" class="form-control" name="P_Code" value="<%=rs.getString("P_Code")%>" required/>
			      </div>
			      <%
                                 session.setAttribute("k_id", String.valueOf(rs.getInt("k_id")));
                                }
                               } catch (Exception ex) {
                                ex.printStackTrace();
                               }
			      %>
			      <button type="submit" class="btn btn-success">Güncelle</button>
			    </form>
			  </div>
			</div>
		      </div>
		    </div>
		  </div>
		</div>
	      </div>
	    </div>
	  </div>

	    <script type="text/javascript" src="../resources/js/required/jquery-1.11.1.min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/bootstrap/bootstrap.min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/jquery.easing.1.3-min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/jquery.mCustomScrollbar.min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/misc/jquery.mousewheel-3.0.6.min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/misc/retina.min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/icheck.min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/misc/jquery.ui.touch-punch.min.js"></script>
	    <script type="text/javascript" src="../resources/js/required/circloid-functions.js"></script>

	    <!-- Optional JS Files -->
	    <script type="text/javascript" src="../resources/js/optional/ckeditor/ckeditor.js"></script>
	    <script type="text/javascript" src="../resources/js/optional/ckeditor/adapters/jquery.js"></script> <!-- This jQuery Adapter is REQUIRED for CKEditor to function properly -->
	    <script type="text/javascript" src="../resources/js/optional/bootstrapValidator.min.js"></script>
	    <!-- <script type="text/javascript" src="../resources/js/optional/bootstrapValidator-language/languagecode_COUNTRYCODE.js"></script> -->
	    <!-- add optional JS plugin files here -->

	    <!-- REQUIRED: User Editable JS Files -->
	    <script type="text/javascript" src="../resources/js/script.js"></script>
	    <!-- add additional User Editable files here -->

	    <!-- Demo JS Files -->
	    <script type="text/javascript" src="../resources/js/demo-files/form-validation.js"></script>
	</body>
	</html>
