
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Ana Sayfa</title>

  <!-- Gerekli CSS Dosyaları -->
  <link type="text/css" href="../resources/css/required/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.structure.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.theme.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/css/required/icheck/all.css" rel="stylesheet" />
      <link type="text/css" href="../resources/fonts/metrize-icons/styles-metrize-icons.css" rel="stylesheet">
      <link type="text/css" href="../resources/css/styles-core.css" rel="stylesheet" />

	<!-- Yedek CSS Dosyaları -->
	<link type="text/css" href="../resources/css/optional/bootstrap-datetimepicker.min.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/demo-files/ui-icons.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/demo-files/pages-invoice.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/demo-files/pages-invoice-print.css" media="print" rel="stylesheet" />

	</head>
	<body>

	  <%

           String uid = String.valueOf(session.getAttribute("id"));
           int u_id = Integer.parseInt(uid);
           Connection con;
           Statement st;
           ResultSet rs;
           con = null;
           st = null;
           rs = null;

           String url = "jdbc:sqlserver://localhost:1433;databaseName=ToyotaProjesi";
           String kullanici = "madmen";
           String parola = "123";

           try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = java.sql.DriverManager.getConnection(url, kullanici, parola);

            String sqlsorgusu = "select * from userInfo where k_id='" + u_id + "'";

            st = con.createStatement();

            rs = st.executeQuery(sqlsorgusu);


	  %>


	<div>
	  <!-- START Right Column -->
	  <div id="right-column">
	    <div class="row hidden-print">
	      <div class="col-md-12">
		<div class="col-md-9"></div>	
		<div class="page-heading-controls col-md-3">
		  <a class="btn btn-primary" role="button" href="javascript:window.print();">Print
		    <span class="glyphicon glyphicon-print"></span>
		  </a>
		  <a href="addNew.jsp?k_id=<%=uid%>" role="button" class="btn btn-danger">Add New</a>
		</div>
	      </div>
	    </div>

	    <div class="row">
	      <div class="col-md-12">
		<div class="block">
		  <div class="block-content-outer">
		    <div class="block-content-inner">
		      <div class="table-responsive">
			<form role="form">
			  <table class="table table-condensed table-striped table-bordered table-hover">
			    <thead>
			      <tr>
				<th class="text-center col-md-1">Bölümü</th>
				<th class="text-center col-md-1">Müdürü</th>
				<th class="text-center col-md-1">Seyehat Başlangıç</th>
				<th class="text-center col-md-1">Seyahat Bitiş</th>
				<th class="text-center col-md-1">Seyehat Yeri</th>
				<th class="text-center col-md-1">Gidiş Amacı</th>
				<th class="text-center col-md-1">Proje Kodu</th>
				<th class="text-center col-md-1">İşlemler</th>
			      </tr>
			    </thead>

			    <%! int i = 0;
                             String renk = "";
			    %>
			    <%            while (rs.next()) {
                              i = i % 2;
                              if (i == 0) {
                               renk = "";
                              } else {
                               renk = "";
                              }
			    %>

			    <tbody>
			      <tr>
				<td class="text-center"><%=rs.getString("Department")%></td>
				<td class="text-center"><%=rs.getString("Manager")%></td>
				<td class="text-center"><%=rs.getString("StartDate")%></td>
				<td class="text-center"><%=rs.getString("EndDate")%></td>
				<td class="text-center"><%=rs.getString("Place")%></td>
				<td class="text-center"><%=rs.getString("Target")%></td>
				<td class="text-center"><%=rs.getString("P_Code")%></td>
				<td class="text-center">
				  <a href="editInfo.jsp?k_id=<%=rs.getInt("id")%>" class="btn btn-success btn-xs" role="button">Edit</a>
				  <a href="delete.jsp?k_id=<%=rs.getInt("id")%>" class="btn btn-success btn-xs" role="button">Delete</a>
				</td>
			      </tr>
			      <%
                                i = i + 1;
                               }
			      %>
			      <%
                               } catch (ClassNotFoundException cnfex) {
                                cnfex.printStackTrace();
                               } finally {
                                if (rs != null) {
                                 rs.close();
                                }
                                if (st != null) {
                                 st.close();
                                }
                                if (con != null) {
                                 con.close();
                                }
                               }
			      %>

			    </tbody>
			  </table>

			  <div class="help-text">1 - 20 Arası gösteriliyor</div>
			  <ul class="pagination">
			    <li class="disabled"><a href="#">&laquo;</a></li>
			    <li class="active"><a href="#"><span>1 <span class="sr-only">(current)</span></span></a></li>
			    <li><a href="#">2</a></li>
			    <li><a href="#">3</a></li>
			    <li><a href="#">&raquo;</a></li>
			  </ul>
			</form>
		      </div>
		    </div>
		  </div>
		</div>
	      </div>
	    </div>
	  </div>

	  <!-- START Footer Container -->
	  <div id="footer-container">
	    <div class="footer-content">
	      &copy; <a href="#?ref=Base5Builder">ToyotaSeyahatProjesi</a> was made with <span style="color:#FF0000;">&#10084;</span> and supported by - <a href="http://base5builder.com/" target="_blank" style="font-weight:300;color:#ffffff;background:#1d1d1d;padding:0 3px;">m&m<span style="color:#ffa733;font-weight:bold"></span></a>
	    </div>
	  </div>
	</div>
	</div>
	</div>
	<!-- Required JS Files -->
	<script type="text/javascript" src="../resources/js/required/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../resources/js/required/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="../resources/js/required/jquery.easing.1.3-min.js"></script>
	<script type="text/javascript" src="../resources/js/required/misc/retina.min.js"></script>
	<script type="text/javascript" src="../resources/js/required/icheck.min.js"></script>
	<script type="text/javascript" src="../resources/js/required/misc/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="../resources/js/required/circloid-functions.js"></script>

	<!-- Optional JS Files -->
	<script type="text/javascript" src="../resources/js/optional/bootstrap-datetimepicker.min.js"></script>

	<!-- REQUIRED: User Editable JS Files -->
	<script type="text/javascript" src="../resources/js/script.js"></script>

	<!-- Demo JS Files -->
	<script type="text/javascript" src="../resources/js/demo-files/ecommerce-order-list.js"></script>


	</body>
	</html>
