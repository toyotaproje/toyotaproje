

<%@page import="java.sql.Connection"%>
<%@page import="db.DBAccess"%>
<%@page import="java.sql.PreparedStatement"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
</head>
<body>
    <%
        String id = request.getParameter("k_id");
        String k_id = String.valueOf(session.getAttribute("id"));
         
        String Department = request.getParameter("Department");
        String Manager = request.getParameter("Manager");
        String StartDate = request.getParameter("StartDate");
        String EndDate = request.getParameter("EndDate");
        String Place = request.getParameter("Place");
        String Target = request.getParameter("Target");
        String P_Code = request.getParameter("P_Code");

        Connection con = null;
        PreparedStatement ps = null;

        try {
            int row = 0;
            con = DBAccess.getConnection();
            String sorgu = "update userInfo set k_id=?, Department=?,Manager=?,StartDate=?,EndDate=?,Place=?,Target=?,P_Code=? where id='"+id+"'";
            
            ps = con.prepareStatement(sorgu);
            ps.setString(1, k_id);
            ps.setString(2, Department);
            ps.setString(3, Manager);
            ps.setString(4, StartDate);
            ps.setString(5, EndDate);
            ps.setString(6, Place);
            ps.setString(7, Target);
            ps.setString(8, P_Code);

            row = ps.executeUpdate();

            if (row> 0) {
                response.sendRedirect("HomePage.jsp");

            } else {
                out.println("Error in query");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    %>
</body>
</html>
