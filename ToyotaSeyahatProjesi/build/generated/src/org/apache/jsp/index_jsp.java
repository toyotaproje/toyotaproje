package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("\n");
      out.write("    <title>Giriş</title>\n");
      out.write("\n");
      out.write("    ");
      out.write("\n");
      out.write("    <link type=\"text/css\" href=\"resources/css/required/bootstrap/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>\n");
      out.write("    <link type=\"text/css\" href=\"resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.css\" rel=\"stylesheet\" />\n");
      out.write("    <link type=\"text/css\" href=\"resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.structure.min.css\" rel=\"stylesheet\" />\n");
      out.write("    <link type=\"text/css\" href=\"resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.theme.min.css\" rel=\"stylesheet\" />\n");
      out.write("    <link type=\"text/css\" href=\"resources/css/required/mCustomScrollbar/jquery.mCustomScrollbar.min.css\" rel=\"stylesheet\" />\n");
      out.write("    <link type=\"text/css\" href=\"resources/css/required/icheck/all.css\" rel=\"stylesheet\" />\n");
      out.write("    <link type=\"text/css\" href=\"resources/fonts/metrize-icons/styles-metrize-icons.css\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("    <!-- More Required CSS Files -->\n");
      out.write("    <link type=\"text/css\" href=\"resources/css/styles-core.css\" rel=\"stylesheet\" />\n");
      out.write("    <link type=\"text/css\" href=\"resources/css/styles-core-responsive.css\" rel=\"stylesheet\" />\n");
      out.write("\n");
      out.write("\n");
      out.write("    <!-- Demo CSS Files -->\n");
      out.write("    <link type=\"text/css\" href=\"resources/css/demo-files/pages-signin-2.css\" rel=\"stylesheet\" />\n");
      out.write("    <link type=\"text/css\" href=\"resources/css/demo-files/pages-signin-signup.css\" rel=\"stylesheet\" />\n");
      out.write("\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body id=\"signin-type-2\">\n");
      out.write("<div class=\"container-fluid\">\n");
      out.write("    <div id=\"body-container\">\n");
      out.write("        <div class=\"standalone-page\">\n");
      out.write("            <div class=\"error-messages hidden\"></div>\n");
      out.write("            <div class=\"standalone-page-content\" data-border-top=\"multi\">\n");
      out.write("                <div class=\"standalone-page-block\">\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-xs-12\">\n");
      out.write("                            <h2 class=\"heading\">\n");
      out.write("                                <span aria-hidden=\"true\" class=\"icon icon-key\"></span>\n");
      out.write("                                <span class=\"main-text\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\tGiriş bilgilerinizi giriniz.\n");
      out.write("\t\t\t\t\t\t\t\t\t</span>\n");
      out.write("                            </h2>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-xs-12\">\n");
      out.write("                            <form role=\"form\" class=\"login-form form-horizontal\" method=\"post\" action=\"jsp/authentication.jsp\">\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <label for=\"inputUserName\" class=\"col-sm-3 control-label\">Username</label>\n");
      out.write("                                    <div class=\"col-sm-9\">\n");
      out.write("                                        <input autocomplete=\"off\" class=\"form-control\" id=\"inputUserName\" placeholder=\"Username\" type=\"text\" name=\"userName\">\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <label for=\"inputPassword\" class=\"col-sm-3 control-label\">Password</label>\n");
      out.write("                                    <div class=\"col-sm-9\">\n");
      out.write("                                        <input autocomplete=\"off\" class=\"form-control\" id=\"inputPassword\" placeholder=\"Password\" type=\"password\" name=\"password\">\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <div class=\"col-sm-offset-3 col-sm-9\">\n");
      out.write("                                        <button id=\"submit-form\" type=\"submit\" class=\"\">Login</button>\n");
      out.write("                                        <a href=\"\" class=\"btn btn-link btn-sm pull-right\">Geri</a>\n");
      out.write("                                        <a href=\"\" class=\"btn btn-link btn-sm pull-right\">I forgot my password</a>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </form>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-xs-12\">\n");
      out.write("                            <div class=\"change-section\">\n");
      out.write("                                <h3 class=\"heading\">Not Registered?</h3>\n");
      out.write("                                <a href=\"\" class=\"btn btn-default btn-block\">Create New Account</a>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</div><!-- /.container -->\n");
      out.write("\n");
      out.write("<!-- Placed at the end of the document so the pages load faster -->\n");
      out.write("<!-- Required JS Files -->\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/jquery-1.11.1.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/bootstrap/bootstrap.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/jquery.easing.1.3-min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/jquery.mCustomScrollbar.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/misc/jquery.mousewheel-3.0.6.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/misc/retina.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/icheck.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/misc/jquery.ui.touch-punch.min.js\"></script>\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/required/circloid-functions.js\"></script>\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- REQUIRED: User Editable JS Files -->\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/script.js\"></script>\n");
      out.write("<!-- add additional User Editable files here -->\n");
      out.write("\n");
      out.write("<!-- Demo JS Files -->\n");
      out.write("<script type=\"text/javascript\" src=\"resources/js/demo-files/pages-signin-2.js\"></script>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
