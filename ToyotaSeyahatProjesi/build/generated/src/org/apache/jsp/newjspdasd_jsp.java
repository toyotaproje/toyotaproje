package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class newjspdasd_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("  <head>\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("    <meta name=\"description\" content=\"\">\n");
      out.write("    <meta name=\"author\" content=\"\">\n");
      out.write("  <link rel=\"icon\" href=\"../resources/images/required/ico/favicon.ico\">\n");
      out.write("    <!-- Fav and touch icons -->\n");
      out.write("    <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"../resources/images/required/ico/apple-touch-icon-144-precomposed.png\">\n");
      out.write("      <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"../resources/images/required/ico/apple-touch-icon-114-precomposed.png\">\n");
      out.write("\t<link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"../resources/images/required/ico/apple-touch-icon-72-precomposed.png\">\n");
      out.write("\t  <link rel=\"apple-touch-icon-precomposed\" href=\"../resources/images/required/ico/apple-touch-icon-57-precomposed.png\">\n");
      out.write("\t    <link rel=\"shortcut icon\" href=\"../resources/images/required/ico/favicon.png\">\n");
      out.write("\n");
      out.write("\t      <title>Circloid - Responsive HTML Admin Template</title>\n");
      out.write("\n");
      out.write("\t      <!-- Required CSS Files -->\n");
      out.write("\t      <link type=\"text/css\" href=\"../resources/css/required/bootstrap/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("\t\t<link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>\n");
      out.write("\t\t  <link type=\"text/css\" href=\"../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t  <link type=\"text/css\" href=\"../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.structure.min.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t  <link type=\"text/css\" href=\"../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.theme.min.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t  <link type=\"text/css\" href=\"../resources/css/required/mCustomScrollbar/jquery.mCustomScrollbar.min.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t  <link type=\"text/css\" href=\"../resources/css/required/icheck/all.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t  <link type=\"text/css\" href=\"../resources/fonts/metrize-icons/styles-metrize-icons.css\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("\t\t    <!-- Optional CSS Files -->\n");
      out.write("\t\t    <link type=\"text/css\" href=\"../resources/css/optional/datatables/css/dataTables.bootstrap.min.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t    <link type=\"text/css\" href=\"../resources/css/optional/datatables/css/dataTables.tableTools.min.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t    <link type=\"text/css\" href=\"../resources/css/optional/datatables/css/dataTables.responsive.min.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t    <!-- add CSS files here -->\n");
      out.write("\n");
      out.write("\t\t    <!-- More Required CSS Files -->\n");
      out.write("\t\t    <link type=\"text/css\" href=\"../resources/css/styles-core.css\" rel=\"stylesheet\" />\n");
      out.write("\t\t    <link type=\"text/css\" href=\"../resources/css/styles-core-responsive.css\" rel=\"stylesheet\" />\n");
      out.write("\n");
      out.write("\t\t    <!-- Demo CSS Files -->\n");
      out.write("\t\t    <link type=\"text/css\" href=\"../resources/css/demo-files/tables-datatables.css\" rel=\"stylesheet\" />\n");
      out.write("\n");
      out.write("\t\t    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->\n");
      out.write("\t\t    <script src=\"../resources/js/required/misc/ie10-viewport-bug-workaround.js\"></script>\n");
      out.write("\n");
      out.write("\t\t    <!--[if IE 7]>\n");
      out.write("\t\t    <link type=\"text/css\" href=\"../resources/css/required/misc/style-ie7.css\" rel=\"stylesheet\">\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/fonts/lte-ie7.js\"></script>\n");
      out.write("\t\t    <![endif]-->\n");
      out.write("\t\t    <!--[if IE 8]>\n");
      out.write("\t\t    <link type=\"text/css\" href=\"../resources/css/required/misc/style-ie8.css\" rel=\"stylesheet\">\n");
      out.write("\t\t    <![endif]-->\n");
      out.write("\t\t    <!--[if lte IE 8]>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/css/required/misc/excanvas.min.js\"></script>\n");
      out.write("\t\t    <![endif]-->\n");
      out.write("\t\t    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->\n");
      out.write("\t\t    <!--[if lt IE 9]>\n");
      out.write("\t\t    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>\n");
      out.write("\t\t    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\n");
      out.write("\t\t    <![endif]-->\n");
      out.write("\t\t    </head>\n");
      out.write("\t\t    <body>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t      <!-- START Search Bar -->\n");
      out.write("\t\t    <div class=\"header-search\"> <!-- NOTE TO READER: Accepts the following class(es) \"animate\" class -->\n");
      out.write("\t\t      <form role=\"form\" class=\"icheck-square\" method=\"post\" action=\"pages-search-results.html\">\n");
      out.write("\t\t\t<ul>\n");
      out.write("\t\t\t  <li>\n");
      out.write("\t\t\t    <a href=\"#\" class=\"search-closed\">\n");
      out.write("\t\t\t      <span aria-hidden=\"true\" class=\"icon icon-search\"></span>\n");
      out.write("\t\t\t      <span class=\"main-text\">Search</span>\n");
      out.write("\t\t\t    </a>\n");
      out.write("\t\t\t    <a href=\"#\" class=\"search-opened\">\n");
      out.write("\t\t\t      <span aria-hidden=\"true\" class=\"icon icon-cross\"></span>\n");
      out.write("\t\t\t      <span class=\"main-text\">Search</span>\n");
      out.write("\t\t\t    </a>\n");
      out.write("\t\t\t    <ul>\n");
      out.write("\t\t\t      <li class=\"simple-search\">\n");
      out.write("\t\t\t\t<div class=\"simple-search-inner\">\n");
      out.write("\t\t\t\t  <div class=\"simple-search-block\">\n");
      out.write("\t\t\t\t    <div class=\"input-group\">\n");
      out.write("\t\t\t\t      <input type=\"text\" class=\"form-control\" id=\"input-search\" placeholder=\"Enter Search Terms...\">\n");
      out.write("\t\t\t\t\t<span class=\"input-group-btn\">\n");
      out.write("\t\t\t\t\t  <button type=\"submit\" class=\"btn btn-default\">\n");
      out.write("\t\t\t\t\t    <span class=\"main-text\">Search</span>\n");
      out.write("\t\t\t\t\t  </button>\n");
      out.write("\t\t\t\t\t</span>\n");
      out.write("\t\t\t\t    </div>\n");
      out.write("\t\t\t\t    <div class=\"checkbox\">\n");
      out.write("\t\t\t\t      <label>\n");
      out.write("\t\t\t\t\t<input type=\"checkbox\" id=\"input-advanced-search\"> Use Advanced Search\n");
      out.write("\t\t\t\t      </label>\n");
      out.write("\t\t\t\t    </div>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t      </li>\n");
      out.write("\t\t\t      <li class=\"advanced-search\">\n");
      out.write("\t\t\t\t<div class=\"advanced-search-block\">\n");
      out.write("\t\t\t\t  <div class=\"form-group\">\n");
      out.write("\t\t\t\t    <label for=\"input-advanced-select\">Advanced Option as dropdown list</label>\n");
      out.write("\t\t\t\t    <select id=\"input-advanced-select\" class=\"form-control\">\n");
      out.write("\t\t\t\t      <option>Advanced Option 1</option>\n");
      out.write("\t\t\t\t      <option>Advanced Option 2</option>\n");
      out.write("\t\t\t\t      <option>Advanced Option 3</option>\n");
      out.write("\t\t\t\t      <option>Advanced Option 4</option>\n");
      out.write("\t\t\t\t    </select>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\n");
      out.write("\t\t\t\t  <div class=\"checkbox\">\n");
      out.write("\t\t\t\t    <label>\n");
      out.write("\t\t\t\t      <input type=\"checkbox\" value=\"\" id=\"input-advanced-checkbox-1\">\n");
      out.write("\t\t\t\t\tMust contain at least one search term\n");
      out.write("\t\t\t\t    </label>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\n");
      out.write("\t\t\t\t  <div class=\"radio\">\n");
      out.write("\t\t\t\t    <label>\n");
      out.write("\t\t\t\t      <input type=\"radio\" name=\"input-advanced-radio-set-1\" id=\"input-advanced-radio-1\" value=\"option1\" checked>\n");
      out.write("\t\t\t\t\tMust only be in \"Themes\" category\n");
      out.write("\t\t\t\t    </label>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t  <div class=\"radio\">\n");
      out.write("\t\t\t\t    <label>\n");
      out.write("\t\t\t\t      <input type=\"radio\" name=\"input-advanced-radio-set-1\" id=\"input-advanced-radio-2\" value=\"option2\">\n");
      out.write("\t\t\t\t\tMust only be in \"Plugins\" category\n");
      out.write("\t\t\t\t    </label>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t      </li>\n");
      out.write("\t\t\t    </ul>\n");
      out.write("\t\t\t  </li>\n");
      out.write("\t\t\t</ul>\n");
      out.write("\t\t      </form>\n");
      out.write("\t\t    </div>\n");
      out.write("\t\t    <!-- END Search Bar -->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t    <!-- Required JS Files -->\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/jquery-1.11.1.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/bootstrap/bootstrap.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/jquery.easing.1.3-min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/jquery.mCustomScrollbar.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/misc/jquery.mousewheel-3.0.6.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/misc/retina.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/icheck.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/misc/jquery.ui.touch-punch.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/required/circloid-functions.js\"></script>\n");
      out.write("\n");
      out.write("\t\t    <!-- Optional JS Files -->\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/optional/datatables/js/jquery.dataTables.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/optional/datatables/js/dataTables.bootstrap.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/optional/datatables/js/dataTables.tableTools.min.js\"></script>\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/optional/datatables/js/dataTables.responsive.min.js\"></script>\n");
      out.write("\t\t    <!-- add optional JS plugin files here -->\n");
      out.write("\n");
      out.write("\t\t    <!-- REQUIRED: User Editable JS Files -->\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/script.js\"></script>\n");
      out.write("\t\t    <!-- add additional User Editable files here -->\n");
      out.write("\n");
      out.write("\t\t    <!-- Demo JS Files -->\n");
      out.write("\t\t    <script type=\"text/javascript\" src=\"../resources/js/demo-files/tables-datatables.js\"></script>\n");
      out.write("\t\t    </body>\n");
      out.write("\t\t    </html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
