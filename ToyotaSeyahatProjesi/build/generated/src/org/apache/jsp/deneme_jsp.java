package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import db.DBAccess;
import java.sql.Connection;

public final class deneme_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("  <head>\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("  <title>JSP Page</title>\n");
      out.write("  <link type=\"text/css\" href=\"resources/css/required/bootstrap/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>\n");
      out.write("      <link type=\"text/css\" href=\"resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.css\" rel=\"stylesheet\" />\n");
      out.write("      <link type=\"text/css\" href=\"resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.structure.min.css\" rel=\"stylesheet\" />\n");
      out.write("      <link type=\"text/css\" href=\"resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.theme.min.css\" rel=\"stylesheet\" />\n");
      out.write("      <link type=\"text/css\" href=\"resources/css/required/mCustomScrollbar/jquery.mCustomScrollbar.min.css\" rel=\"stylesheet\" />\n");
      out.write("      <link type=\"text/css\" href=\"resources/css/required/icheck/all.css\" rel=\"stylesheet\" />\n");
      out.write("      <link type=\"text/css\" href=\"resources/fonts/metrize-icons/styles-metrize-icons.css\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("\t<!-- Optional CSS Files -->\n");
      out.write("\t<link type=\"text/css\" href=\"resources/css/optional/bootstrapValidator.min.css\" rel=\"stylesheet\" />\n");
      out.write("\t<!-- add CSS files here -->\n");
      out.write("\n");
      out.write("\t<!-- More Required CSS Files -->\n");
      out.write("\t<link type=\"text/css\" href=\"resources/css/styles-core.css\" rel=\"stylesheet\" />\n");
      out.write("\t<link type=\"text/css\" href=\"resources/css/styles-core-responsive.css\" rel=\"stylesheet\" />\n");
      out.write("\t</head>\n");
      out.write("\t");


         String k_idS = request.getParameter("k_id");

         int k_id = 0;

         if (k_idS == null || k_idS.isEmpty()) {
          response.sendRedirect("index.jsp");
         } else {
          k_id = Integer.parseInt(k_idS);

         }

         Connection con = null;
         con = DBAccess.getConnection();

	
      out.write("\n");
      out.write("\t<body>\n");
      out.write("\t  <div class=\"row\">\n");
      out.write("\t    <div class=\"col-md-6\">\n");
      out.write("\t      <div class=\"row\">\n");
      out.write("\t\t<div class=\"col-md-12\">\n");
      out.write("\t\t  <div class=\"block\">\n");
      out.write("\t\t    <div class=\"block-heading\">\n");
      out.write("\t\t      <div class=\"main-text h2\">\n");
      out.write("\t\t\tYeni Kayıt\n");
      out.write("\t\t      </div>\n");
      out.write("\t\t      <div class=\"block-controls\">\n");
      out.write("\t\t\t<span aria-hidden=\"true\" class=\"icon icon-cross icon-size-medium block-control-remove\"></span>\n");
      out.write("\t\t\t<span aria-hidden=\"true\" class=\"icon icon-arrow-down icon-size-medium block-control-collapse\"></span>\n");
      out.write("\t\t      </div>\n");
      out.write("\t\t    </div>\n");
      out.write("\t\t    <div class=\"block-content-outer\">\n");
      out.write("\t\t      <div class=\"block-content-inner\">\n");
      out.write("\t\t\t<form id=\"registrationForm\" method=\"post\" action=\"jsp/addNewBack.jsp?k_id=");
      out.print(k_id);
      out.write("\">\n");
      out.write("\t\t\t  <div class=\"form-group\">\n");
      out.write("\t\t\t    <label class=\"control-label\">Bölümü</label>\n");
      out.write("\t\t\t    <input type=\"text\" class=\"form-control\" name=\"Department\" />\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t  <div class=\"form-group\">\n");
      out.write("\t\t\t    <label class=\"control-label\">Müdürü</label>\n");
      out.write("\t\t\t    <input type=\"text\" class=\"form-control\" name=\"Manager\" />\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t  <div class=\"form-group\">\n");
      out.write("\t\t\t    <label class=\"control-label\">Seyahat Başlangıç</label>\n");
      out.write("\t\t\t    <input type=\"date\" class=\"form-control\" name=\"StartDate\" placeholder=\"DD/MM/YYYY\" />\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t  <div class=\"form-group\">\n");
      out.write("\t\t\t    <label class=\"control-label\">Seyahat Bitiş</label>\n");
      out.write("\t\t\t    <input type=\"date\" class=\"form-control\" name=\"EndDate\" placeholder=\"DD/MM/YYYY\" />\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t  <div class=\"form-group\">\n");
      out.write("\t\t\t    <label class=\"control-label\">Seyahat Yeri</label>\n");
      out.write("\t\t\t    <input type=\"text\" class=\"form-control\" name=\"Place\" />\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t  <div class=\"form-group\">\n");
      out.write("\t\t\t    <label class=\"control-label\">Gidiş Amacı</label>\n");
      out.write("\t\t\t    <input type=\"text\" class=\"form-control\" name=\"Target\" />\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t  <div class=\"form-group\">\n");
      out.write("\t\t\t    <label class=\"control-label\">Proje Kodu</label>\n");
      out.write("\t\t\t    <input type=\"text\" class=\"form-control\" name=\"P_Code\" />\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t  <button type=\"submit\" class=\"btn btn-success\">Kaydet</button>\n");
      out.write("\t\t\t</form>\n");
      out.write("\t\t      </div>\n");
      out.write("\t\t    </div>\n");
      out.write("\t\t  </div>\n");
      out.write("\t\t</div>\n");
      out.write("\t      </div>\n");
      out.write("\t    </div>\n");
      out.write("\t  </div>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/jquery-1.11.1.min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/bootstrap/bootstrap.min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/jquery.easing.1.3-min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/jquery.mCustomScrollbar.min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/misc/jquery.mousewheel-3.0.6.min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/misc/retina.min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/icheck.min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/misc/jquery.ui.touch-punch.min.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/required/circloid-functions.js\"></script>\n");
      out.write("\n");
      out.write("\t<!-- Optional JS Files -->\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/optional/ckeditor/ckeditor.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/optional/ckeditor/adapters/jquery.js\"></script> <!-- This jQuery Adapter is REQUIRED for CKEditor to function properly -->\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/optional/bootstrapValidator.min.js\"></script>\n");
      out.write("\t<!-- <script type=\"text/javascript\" src=\"resources/js/optional/bootstrapValidator-language/languagecode_COUNTRYCODE.js\"></script> -->\n");
      out.write("\t<!-- add optional JS plugin files here -->\n");
      out.write("\n");
      out.write("\t<!-- REQUIRED: User Editable JS Files -->\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/script.js\"></script>\n");
      out.write("\t<!-- add additional User Editable files here -->\n");
      out.write("\n");
      out.write("\t<!-- Demo JS Files -->\n");
      out.write("\t<script type=\"text/javascript\" src=\"resources/js/demo-files/form-validation.js\"></script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t</body>\n");
      out.write("\t</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
