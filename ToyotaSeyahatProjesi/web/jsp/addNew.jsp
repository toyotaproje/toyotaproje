
<%@page import="db.DBAccess"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Yeni Ekle</title>
  <link type="text/css" href="../resources/css/required/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.structure.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.theme.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/css/required/mCustomScrollbar/jquery.mCustomScrollbar.min.css" rel="stylesheet" />
      <link type="text/css" href="../resources/css/required/icheck/all.css" rel="stylesheet" />
      <link type="text/css" href="../resources/fonts/metrize-icons/styles-metrize-icons.css" rel="stylesheet">

	<!-- Optional CSS Files -->
	<link type="text/css" href="../resources/css/optional/bootstrapValidator.min.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/optional/switchery.min.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/optional/powerange.min.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/optional/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/optional/bootstrap-tagsinput.min.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/optional/bootstrap-datetimepicker.min.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/optional/bootstrap-multiselect.min.css" rel="stylesheet" />
	<!-- add CSS files here -->

	<!-- More Required CSS Files -->
	<link type="text/css" href="../resources/css/styles-core.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/styles-core-responsive.css" rel="stylesheet" />
	<link type="text/css" href="../resources/css/demo-files/form-extras.css" rel="stylesheet" />


	</head>
	<%

         String k_idS = request.getParameter("k_id");

         int k_id = 0;

         if (k_idS == null || k_idS.isEmpty()) {
          response.sendRedirect("../index.jsp");
         } else {
          k_id = Integer.parseInt(k_idS);

         }

         Connection con = null;
         con = DBAccess.getConnection();

	%>
	<body>
	<div id="right-column">
	  <div class="col-md-12">
	    <div class="col-md-4"></div>
	    <div class="row">
	      <div class="col-md-4">
		<div class="block">
		  <div class="block-heading">
		    <div class="main-text h3">
		      Yeni Kayıt
		    </div>
		    <div class="block-controls">
		      <span aria-hidden="true" class="icon icon-arrow-down icon-size-medium block-control-collapse"></span>
		    </div>
		  </div>

		  <div class="block-content-outer">
		    <div class="block-content-inner">

		      <form id="registrationForm registrationForm-1" method="post" action="addNewBack.jsp?k_id=<%=k_id%>">
			<div class="form-group">
			  <label class="control-label">Bölümü</label>
			  <input type="text" class="form-control" name="Department" />
			</div>
			<div class="form-group">
			  <label class="control-label">Müdürü</label>
			  <input type="text" class="form-control" name="Manager" />
			</div>

			<div class="form-group">
			  <label class="control-label">Seyahat Başlangıç</label>
			  <div class="input-group" id="datetimepicker5">
			    <input type="text" name="StartDate" class="form-control" autocomplete="off" placeholer="DD/MM/YYYY">
			      <span class="input-group-addon input-group-icon">
				<span class="glyphicon glyphicon-calendar"></span>
			      </span>
			  </div>
			</div>

			<div class="form-group">
			  <label class="control-label">Seyahat Bitiş</label>
			  <div class="input-group" id="datetimepicker6">
			    <input type="text" name="EndDate" class="form-control" autocomplete="off" placeholer="DD/MM/YYYY">
			      <span class="input-group-addon input-group-icon">
				<span class="glyphicon glyphicon-calendar"></span>
			      </span>
			  </div>
			</div>

			<%--<div class="form-group">
			  <label class="control-label">Seyahat Başlangıç</label>
			  <input type="date" class="form-control" name="StartDate" placeholder="DD/MM/YYYY" />
			</div>
			<div class="form-group">
			  <label class="control-label">Seyahat Bitiş</label>
			  <input type="date" class="form-control" name="EndDate" placeholder="DD/MM/YYYY" />
			</div>--%>

			<div class="form-group">
			  <label class="control-label">Seyahat Yeri</label>
			  <input type="text" class="form-control" name="Place" />
			</div>
			<div class="form-group">
			  <label class="control-label">Gidiş Amacı</label>
			  <input type="text" class="form-control" name="Target" />
			</div>
			<div class="form-group">
			  <label class="control-label">Proje Kodu</label>
			  <input type="text" class="form-control" name="P_Code" />
			</div>
			<button type="submit" class="btn btn-success">Kaydet</button>
		      </form>
		    </div>
		  </div>
		</div>
	      </div>
	    </div>
	  </div>


	  <script type="text/javascript" src="../resources/js/required/jquery-1.11.1.min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/bootstrap/bootstrap.min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/jquery.easing.1.3-min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/jquery.mCustomScrollbar.min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/misc/jquery.mousewheel-3.0.6.min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/misc/retina.min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/icheck.min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/misc/jquery.ui.touch-punch.min.js"></script>
	  <script type="text/javascript" src="../resources/js/required/circloid-functions.js"></script>

	  <!-- Optional JS Files -->
	  <script type="text/javascript" src="../resources/js/optional/jquery.mask.min.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/switchery.min.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/powerange.min.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/jquery.bootstrap-touchspin.min.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/bootstrap-tagsinput.min.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/misc/typeahead.bundle.min.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/misc/moment.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/bootstrap-datetimepicker.min.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/bootstrap-multiselect.min.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/ckeditor/ckeditor.js"></script>
	  <script type="text/javascript" src="../resources/js/optional/ckeditor/adapters/jquery.js"></script> <!-- This jQuery Adapter is REQUIRED for CKEditor to function properly -->
	  <!-- add optional JS plugin files here -->

	  <!-- REQUIRED: User Editable JS Files -->
	  <script type="text/javascript" src="../resources/js/script.js"></script>
	  <!-- add additional User Editable files here -->

	  <!-- Demo JS Files -->
	  <script type="text/javascript" src="../resources/js/demo-files/form-extras.js"></script>


	  </body>
	  </html>
