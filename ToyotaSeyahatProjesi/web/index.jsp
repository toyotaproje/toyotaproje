
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>

  <title>Giriş</title>

  <%-- Gerekli CSS Dosyaları--%>
  <link type="text/css" href="resources/css/required/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link type="text/css" href="resources/css/required/icheck/all.css" rel="stylesheet" />
      <link type="text/css" href="resources/fonts/metrize-icons/styles-metrize-icons.css" rel="stylesheet">

	<!-- More Required CSS Files -->
	<link type="text/css" href="resources/css/styles-core.css" rel="stylesheet" />

	<!-- Demo CSS Files -->
	<link type="text/css" href="resources/css/demo-files/pages-signin-2.css" rel="stylesheet" />
	<link type="text/css" href="resources/css/demo-files/pages-signin-signup.css" rel="stylesheet" />

	</head>
	<body id="signin-type-2">
	<div class="container-fluid">
	  <div id="body-container">
	    <div class="standalone-page">
	      <div class="error-messages hidden"></div>
	      <div class="standalone-page-content" data-border-top="multi">
                <div class="standalone-page-block">
		  <div class="row">
		    <div class="col-xs-12">
		      <h2 class="heading">
			<span aria-hidden="true" class="icon icon-key"></span>
			<span class="main-text h3">
			  Please enter your login details.
			</span>
		      </h2>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-xs-12">
		      <form role="form" class="login-form form-horizontal" method="post" action="jsp/authentication.jsp">
			<div class="form-group">
			  <label for="inputUserName" class="col-sm-3 control-label">Username</label>
			  <div class="col-sm-9">
			    <input autocomplete="off" class="form-control" id="inputUserName" placeholder="Username" type="text" name="userName">
			  </div>
			</div>
			<div class="form-group">
			  <label for="inputPassword" class="col-sm-3 control-label">Password</label>
			  <div class="col-sm-9">
			    <input autocomplete="off" class="form-control" id="inputPassword" placeholder="Password" type="password" name="password">
			  </div>
			</div>
			<div class="form-group">
			  <div class="col-sm-offset-3 col-sm-9">
			    <button id="submit-form" type="submit" class="">Login</button>
			    <a href="" class="btn btn-link btn-sm pull-right">Back</a>
			    <a href="" class="btn btn-link btn-sm pull-right">I forgot my password</a>
			  </div>
			</div>
		      </form>
		    </div>
		  </div>
		  <div class="row">
		    <div class="col-xs-12">
		      <div class="change-section">
			<h3 class="heading">Not Registered?</h3>
			<a href="" class="btn btn-default btn-block">Create New Account</a>
		      </div>
		    </div>
		  </div>
                </div>
	      </div>
	    </div>
	  </div>
	</div><!-- /.container -->

	<!-- Placed at the end of the document so the pages load faster -->
	<!-- Required JS Files -->
	<script type="text/javascript" src="resources/js/required/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="resources/js/required/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script>
	<script type="text/javascript" src="resources/js/required/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/js/required/jquery.easing.1.3-min.js"></script>
	<script type="text/javascript" src="resources/js/required/misc/retina.min.js"></script>
	<script type="text/javascript" src="resources/js/required/icheck.min.js"></script>
	<script type="text/javascript" src="resources/js/required/misc/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="resources/js/required/circloid-functions.js"></script>


	<!-- REQUIRED: User Editable JS Files -->
	<script type="text/javascript" src="resources/js/script.js"></script>
	<!-- add additional User Editable files here -->

	<!-- Demo JS Files -->
	<script type="text/javascript" src="resources/js/demo-files/pages-signin-2.js"></script>

	</body>
	</html>
